export interface EmployeeInterface {

  id: string;
  establishmentId: string;
  firstname: string;
  lastname: string;

  email: string;
  role: string;

  avatarImgUrl: string;
  creationDate: string;
  modificationDate: string;
  isDisabled: boolean;
  canChat: boolean;
  isOnline: boolean;

}
