export interface GeolocationInterface {
  lat: string;
  lon: string;
}
