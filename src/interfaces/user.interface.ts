import { AddressInterface } from './address.interface';

export interface UserInterface {
  elId: string;
  id: string;
  firstname: string;
  lastname: string;

  email: string;
  password: string;
  mobile: Number;
  adress: AddressInterface;

  avatarImgUrl: string;
  creationDate: Number;
  modificationDate: Number;
  isActive: boolean;
  isOnline: boolean;
}
