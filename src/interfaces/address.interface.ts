export interface AddressInterface {
  streetName: string;
  streetNumber: Number;
  city: string;
  zipCode: string;
  region: string;
  country: string;
}
