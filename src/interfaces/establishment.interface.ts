import { GeolocationInterface } from './geolocation.interface';
import { AddressInterface } from './address.interface';
import { EmployeeInterface } from './employee.interface';
import {DescriptionInterface} from './description.interface';

export interface EstablishmentInterface {
  id: string;
  brandCode: string;
  name: string;
  description: DescriptionInterface;

  email: string;
  password: string;
  phone: Number;
  mobile: Number;
  website: string;

  address: AddressInterface;
  geolocation: GeolocationInterface;
  distance: string;

  presentationImgUrl: string;
  employees: Array<EmployeeInterface>;
  albumImgUrls: Array<string>;
  tags: Array<string>;

  creationDate: string;
  modificationDate: string;
  isActive: boolean;
  // todo : add the following : upvotes - downvotes
}
