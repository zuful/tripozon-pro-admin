import { EstablishmentInterface } from './establishment.interface';

interface CompanyInterface {
  id: string;
  companyName: string;

    shops: Array<EstablishmentInterface>;

  creationDate: Number;
  modificationDate: Number;
  isActive: boolean;
}
