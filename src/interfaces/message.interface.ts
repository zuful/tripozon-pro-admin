export interface MessageInterface {
    id: string;
    expeditorId: string;
    message: string;
    creationDate: string;
    seen: boolean;
}
