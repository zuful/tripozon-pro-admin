export interface TouristicServiceInterface {
    icon: string
    name: string
    imgUrl: string
}
