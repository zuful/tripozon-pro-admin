export interface EmployeeFireauthInterface {

  id: string;
  firstname: string;
  lastname: string;

  email: string;

  avatarImgUrl: string;
  creationDate: string;
  modificationDate: string;
  disabled: boolean;

}
