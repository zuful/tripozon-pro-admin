import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { EstablishmentInterface } from '../interfaces/establishment.interface';
import { EmployeeInterface } from '../interfaces/employee.interface';
import { ActivatedRoute, Router } from '@angular/router';

// TODO: Fetch all of the establishement for one brand
// TODO: Create a page with all of the establishments of a brand
/*
  https://identity.apaleo.com/connect/authorize?response_type=code&scope=properties.read+units.read+unitgroups.read+settings.read+companies.read+rateplans.read-corporate+rateplans.read+services.read+availability.read+offers.read+account.read&client_id=TripozonConnect&redirect_uri=https%3A%2F%2Ftripozon-pro-admin.firebaseapp.com%2F&state=standard
 */
@Injectable()
export class EstablishmentService {
  public status = '';
  private serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';
  constructor(private http: HttpClient, private activatedRoute: ActivatedRoute, private router: Router) {

  }

  editEstablishment(establishment: EstablishmentInterface) {

    establishment.modificationDate = new Date(Date.now()).toISOString();

    return this.http.patch(this.serviceBaseUrl + '/establishment/' + establishment.id,
      establishment,
      {headers: new HttpHeaders().set('Content-Type', 'application/json')});

  }

  checkForm(formFields: Array<any>) {}

  getEmployee(establishmentId: string, employeeId: string) {

    const url = this.serviceBaseUrl + '/employee/' + establishmentId + '/' + employeeId;

    return this.http.get(url).subscribe( employeeFromFirestore => {
      return employeeFromFirestore as EmployeeInterface;
    }, err => {
      console.log(err);
    } );
  }

  isLoggedIn(): boolean {

    let isLoggedIn: boolean;
    const jsonEmployee: string = localStorage.getItem('allEstablishments');
    const employee: EmployeeInterface = JSON.parse(jsonEmployee) as EmployeeInterface;

    if ((employee === null || employee === undefined)) {
      isLoggedIn = false;
    } else {
      isLoggedIn = true;
    }

    return isLoggedIn;
  }

  apaleoConnect(code: string) {
    const appaleoConnectUrl = this.serviceBaseUrl + '/apaleo-establishments/' + code;
    this.status = 'pending';

    this.http.get(appaleoConnectUrl, {headers: new HttpHeaders().set('Content-Type', 'application/x-www-form-urlencoded')})
      .subscribe(allEstablishments => {
        const jsonAllEstablishments = JSON.stringify(allEstablishments);
        localStorage.removeItem('establishmentId'); // if the infos of a previous connexion to a different establishement exists
        localStorage.setItem('allEstablishments', jsonAllEstablishments);
        localStorage.setItem('pms', 'apaleo');

        this.status = '';
        this.router.navigate(['/establishment-list/' + localStorage.getItem('pms')]);
      }, err => {
        this.status = '';
        console.log(err);
    });
  }
}
