import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { Routes, RouterModule } from '@angular/router';
import {
  MatButtonModule, MatCardModule, MatDialogModule, MatDividerModule, MatFormFieldModule, MatIconModule, MatInputModule, MatListModule,
  MatOptionModule,
  MatProgressBarModule, MatSelectModule, MatSnackBarModule,
  MatToolbarModule
} from '@angular/material';
import { NavigationBarComponent } from './navigation-bar/navigation-bar.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { StaffComponent } from './staff/staff.component';
import { EstablishmentFormComponent } from './establishment-form/establishment-form.component';
import { EmployeeFormComponent } from './employee-form/employee-form.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { Md5 } from 'ts-md5';
import { ImagesComponent } from './images/images.component';
import { EstablishmentService } from '../providers/establishment-service';
import { LoginComponent } from './login/login.component';
import { AngularFireModule} from 'angularfire2';
import { AngularFireAuth} from 'angularfire2/auth';
import { EstablishmentListComponent } from './establishment-list/establishment-list.component';
import { EstablishmentDetailComponent } from './establishment-detail/establishment-detail.component';
import { PrivacyComponent } from './privacy/privacy.component';

/*
local strorage variables:

establishmentId
employee
allEstablishments
 */
// TODO: make a redirection to establishment list for everyone (pms or not)
// TODO: add the pms field to the creation a new establishment from a pms
// TODO: create a default employee when an establishment is created from a pms

const appRoutes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: '', component: HomeComponent },
  { path: 'home', component: HomeComponent },
  { path: 'privacy', component: PrivacyComponent },
  { path: 'establishment-detail/:establishmentId', component: EstablishmentDetailComponent },
  { path: 'establishment-list/:pms', component: EstablishmentListComponent },
  { path: 'staff/:establishmentId', component: StaffComponent },
  { path: 'images/:establishmentId', component: ImagesComponent }
];

const firebaseConfig = {
  apiKey: 'AIzaSyBR09wbWml1WDhdqzMAroC005l-_zKt-zM',
  authDomain: 'messenger-1670c.firebaseapp.com',
  databaseURL: 'https://messenger-1670c.firebaseio.com',
  projectId: 'messenger-1670c',
  storageBucket: '',
  messagingSenderId: '292901996795'
};

@NgModule({
  declarations: [
    AppComponent,
    NavigationBarComponent,
    HomeComponent,
    StaffComponent,
    EstablishmentFormComponent,
    EmployeeFormComponent,
    ImagesComponent,
    LoginComponent,
    EstablishmentListComponent,
    EstablishmentDetailComponent,
    PrivacyComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    MatToolbarModule,
    MatCardModule,
    MatButtonModule,
    MatDividerModule,
    MatListModule,
    MatIconModule,
    MatDialogModule,
    MatFormFieldModule,
    MatInputModule,
    FormsModule,
    MatProgressBarModule,
    HttpClientModule,
    MatSelectModule,
    MatOptionModule,
    MatSnackBarModule,
    RouterModule.forRoot(appRoutes),
    AngularFireModule.initializeApp(firebaseConfig),
  ],
  entryComponents: [EstablishmentFormComponent, EmployeeFormComponent],
  providers: [Md5, EstablishmentService, AngularFireAuth],
  bootstrap: [AppComponent]
})
export class AppModule { }
