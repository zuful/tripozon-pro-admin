import { Component, Inject } from '@angular/core';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import { EmployeeInterface } from '../../interfaces/employee.interface';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Md5 } from 'ts-md5';

@Component({
  selector: 'app-employee-form',
  templateUrl: './employee-form.component.html',
  styleUrls: ['./employee-form.component.css']
})
export class EmployeeFormComponent {

  public serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';
  public employee: EmployeeInterface;

  constructor(public dialogRef: MatDialogRef<EmployeeFormComponent>,
              @Inject(MAT_DIALOG_DATA) public data: any,
              private http: HttpClient,
              private md5: Md5) {
    this.employee = this.data.value;
  }

  action() {

    if (this.data.action === 'add') {
      this.addEmployee();
    } else if (this.data.action === 'edit') {
      this.editEmployee();
    }

  }

  addEmployee() {
    if (this.checkEmployeeForm() === true) {

      const date = new Date(Date.now()).toISOString();

      this.employee.avatarImgUrl = this.getGravatarUrl(this.employee.email, '100');
      this.employee.establishmentId = this.data.establishmentId;
      this.employee.creationDate = date;
      this.employee.modificationDate = date;

      this.http.post(this.serviceBaseUrl + '/employee/' + this.data.establishmentId,
        this.employee,
        {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe( output => {
        this.dialogRef.close('add');
      }, err => {
        console.log(err);
      });

    } else {
      alert('All fields must be filled.');
    }
  }

  editEmployee() {

    if (this.checkEmployeeForm() === true) {

      this.employee.modificationDate = new Date(Date.now()).toISOString();
      console.log('inside condition');

      this.http.patch(this.serviceBaseUrl + '/employee/' + this.data.establishmentId,
        this.employee,
        {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe(output => {
        this.dialogRef.close('edit');
      }, err => {
          console.log(err);
      });

    } else {
      alert('All fields must be filled.');
    }

  }

  checkEmployeeForm(): boolean {
    let isFormOk: boolean;

    if (this.employee.firstname !== undefined && this.employee.firstname !== ''
      && this.employee.lastname !== undefined && this.employee.lastname !== ''
      && this.employee.email !== undefined && this.employee.email !== ''
      && this.employee.role !== undefined && this.employee.role !== '') {
      isFormOk = true;
    } else {
      isFormOk = false;
    }

    return isFormOk;
  }

  getGravatarUrl(email: string, size: string) {

    let gravatarUrl: string;
    const gravatarBaseUrl = 'https://www.gravatar.com/avatar/';
    const md5Email = this.md5.appendStr(email.toLowerCase().trim()).end();

    if (size === '0') {
      gravatarUrl = gravatarBaseUrl + md5Email + '?s=';
    } else {
      gravatarUrl = gravatarBaseUrl + md5Email + '?s=' + size;
    }

    return gravatarUrl;
  }

}
