import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { EstablishmentService } from '../../providers/establishment-service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent {
  constructor(public establishmentService: EstablishmentService, private activatedRoute: ActivatedRoute, private router: Router) {
    this.activatedRoute.queryParams.subscribe(params => {
      const code = params['code'];

      if (code !== undefined) { // check for the url variables if there's "code" in it, it's apaleo
        this.establishmentService.apaleoConnect(code);
      } else {

        if (this.establishmentService.isLoggedIn() === true) {
          this.router.navigate(['/establishment-list/' + localStorage.getItem('pms')]);
        } else {
          this.router.navigate(['/login']);
        }

      }

    });
  }

}
