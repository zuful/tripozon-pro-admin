import { Component, Inject } from '@angular/core';
import { DOCUMENT } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';
import { EstablishmentInterface } from '../../interfaces/establishment.interface';
import { EstablishmentService } from '../../providers/establishment-service';

@Component({
  selector: 'app-establishment-list',
  templateUrl: './establishment-list.component.html',
  styleUrls: ['./establishment-list.component.css']
})
export class EstablishmentListComponent  {
  establishmentList: Array<EstablishmentInterface> = JSON.parse(localStorage.getItem('allEstablishments')) as Array<EstablishmentInterface>;

  constructor(@Inject(DOCUMENT) private document: any,
              private route: ActivatedRoute,
              private establishmentService: EstablishmentService,
              private router: Router) {

    const pms = this.route.snapshot.paramMap.get('pms');

    if (this.establishmentService.isLoggedIn() === false && pms === 'apaleo') {
      const urlApaleoAuthenticate = 'https://identity.apaleo.com/connect/authorize?response_type=code&' +
        'scope=properties.read+settings.read+companies.read+account.read&client_id=TripozonConnect' +
        '&redirect_uri=https%3A%2F%2Ftripozon-pro-admin.firebaseapp.com%2F&state=' + Math.random();

      document.location.href = urlApaleoAuthenticate;
    } else if (this.establishmentService.isLoggedIn() === false) {
      this.router.navigate(['/establishment-list/default']);
    }

  }

  getAllEstablishment() {// TODO: delete after tests
    return JSON.parse('[{"id":"b156080f-8486-4198-a945-b1515f46dc3b","brandId":"QGLM","name":"Hotel Berlin",' +
      '"description":{"en":"Explore the important places of recent history or simply enjoy the view over the rooftops of Berlin Mitte ' +
      'on our roof terrace."},"address":{"streetName":"Friedrichstraße 79-80","city":"Berlin","zipCode":"10117","country":"DE"},' +
      '"currencyCode":"EUR","isActive":true},{"id":"c34911ee-7232-4d60-850c-1b0b6b2c8f00","brandId":"QGLM","name":"Hotel Munich",' +
      '"description":{"en":"This new cozy hotel is located in the heart of Schwabing and is walking distance from the historical city ' +
      'center."},"address":{"streetName":"Leopoldstraße 8-10","city":"Munich","zipCode":"80802","country":"DE"},"currencyCode":"EUR",' +
      '"isActive":true},{"id":"c37f97e3-8a72-49f2-9cb3-a60393f6dc89","brandId":"QGLM","name":"Hotel London","description":' +
      '{"en":"Located in a quiet side street with a beautiful garden in the backyard, Hotel London is prefect for relaxing after' +
      ' a day of shopping or a night of partying."},"address":{"streetName":"James Street 5","city":"London","zipCode":"WC2E 8NS",' +
      '"country":"GB"},"currencyCode":"GBP","isActive":true}]') as Array<EstablishmentInterface>;
  }

}
