import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Router } from '@angular/router';
import { EstablishmentService } from '../../providers/establishment-service';
import { EmployeeInterface } from '../../interfaces/employee.interface';
import { MatSnackBar } from '@angular/material';
import { AngularFireAuth } from 'angularfire2/auth';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent {

  public status = '';
  private serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';

  public establishmentId: string;
  public email: string;
  public password: string;

  constructor(private http: HttpClient,
              private router: Router,
              private establishmentService: EstablishmentService,
              private snackbar: MatSnackBar,
              private afa: AngularFireAuth) {

  }

  login() {
    if (this.establishmentId === undefined || this.establishmentId === '' ||
      this.email === undefined || this.email === '' ||
      this.password === undefined || this.password === '') {

      alert('All fields must be filled.');

    } else {

      this.status = 'pending';
      let checkOk: boolean;
      let msg: string;

      this.afa.auth.signInWithEmailAndPassword(this.email, this.password).then( employeeCredentials => {

        if (employeeCredentials === null || employeeCredentials === undefined) {// check if the user is found

          checkOk = false;
          msg = 'User not found.';

        } else if (employeeCredentials.user.emailVerified !== true) {// check if the email is verifyied

          checkOk = false;
          msg = 'E-mail address not verified.';

        } else {
          checkOk = true;
        }

        if (checkOk !== true) {

          this.status = 'failure';
          this.snackbar.open(msg, 'understood', {
            duration: 3000
          });

        } else {

          const url = this.serviceBaseUrl + '/employee/' + this.establishmentId + '/' + employeeCredentials.user.uid;
          let employee: EmployeeInterface;

          this.http.get(url).subscribe( employeeFromFirestore => {// persists infos in local storage then redirect to homepage

            employee = employeeFromFirestore as EmployeeInterface;

            if (employee.role !== 'admin') {

              this.status = 'failure';
              this.snackbar.open('Access denied: insufficient rights', 'understood', {
                duration: 3000
              });

            } else {

              console.log(employee);
              localStorage.setItem('employee', JSON.stringify(employee));

              this.status = 'success';
              this.router.navigate(['/']);

            }

          }, err => {
            console.log(err);
          } );

        }

      });

    }
  }
}
