import { Component } from '@angular/core';
import { EmployeeInterface } from '../../interfaces/employee.interface';
import { MatDialog, MatSnackBar } from '@angular/material';
import { EmployeeFormComponent } from '../employee-form/employee-form.component';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {EstablishmentService} from '../../providers/establishment-service';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-staff',
  templateUrl: './staff.component.html',
  styleUrls: ['./staff.component.css']
})
export class StaffComponent {

  public status = '';
  public serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';
  private establishmentId = '';
  public emptyEmployee: EmployeeInterface = {} as EmployeeInterface; // used when we want to create a new employee
  public allEmployees: Array<EmployeeInterface>;

  constructor(private http: HttpClient,
              private dialog: MatDialog,
              private snackbar: MatSnackBar,
              private establishmentService: EstablishmentService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {

    if (this.establishmentService.isLoggedIn() === true) {

      this.establishmentId = this.activatedRoute.snapshot.paramMap.get('establishmentId');
      this.setAllEmployees();

    } else {
      this.router.navigate(['/login']);
    }

  }

  setAllEmployees() {

    this.status = 'pending';

    this.http.get(this.serviceBaseUrl + '/all-employees/' + this.establishmentId).subscribe( resAllEmployees => {

      this.allEmployees = resAllEmployees as Array<EmployeeInterface>;
      this.status = 'success';

    });

  }

  openModal(action: string, employee: EmployeeInterface) {

    const dialogRef = this.dialog.open(EmployeeFormComponent, {
      width: '700px',
      data: {action: action, establishmentId: this.establishmentId, value: employee}
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {
        this.setAllEmployees();

        let msg: string;
        if (result === 'add') {
          msg = 'Employee successfully added.';
        } else if (result === 'edit') {
          msg = 'Employee successfully edited.';
        }

        console.log('The dialog was closed');
        this.snackbar.open(msg, 'understood', {
          duration: 2500
        });
      }

    });
  }

  employeeCanChat(employee: EmployeeInterface) { // todo: take in consideration the firebase auth active

    employee.canChat = (employee.canChat === true) ? false : true;

    this.http.patch(this.serviceBaseUrl + '/employee/' + this.establishmentId,
      employee,
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe(output => {

      this.setAllEmployees();
      const msg = (employee.canChat === true) ? 'Authorization to chat activated' : 'Authorization to chat deactivated';
      this.snackbar.open(msg, 'understood', {
        duration: 2500
      });

    }, err => {
      console.log(err);
    });

  }

  toggleDisableEmployee(employeeId: string) {

    this.http.post(this.serviceBaseUrl + '/toggleDisableEmployee',
      {employeeId: employeeId},
      {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe(isDisabled => {

      this.setAllEmployees();
      let msg: string;
      if (isDisabled === true) {
        msg = 'Employee account deactivated.';

      } else if (isDisabled === false) {
        msg = 'Employee account activated.';
      }

      this.snackbar.open(msg, 'understood', {
        duration: 2500
      });

    }, err => {
      console.log(err);
    });

  }

}
