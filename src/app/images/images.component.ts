import { Component } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { EstablishmentInterface } from '../../interfaces/establishment.interface';
import { EstablishmentService } from '../../providers/establishment-service';
import { MatSnackBar } from '@angular/material';
import {ActivatedRoute, Router} from '@angular/router';

@Component({
  selector: 'app-images',
  templateUrl: './images.component.html',
  styleUrls: ['./images.component.css']
})
export class ImagesComponent {

  public status = '';
  public imageUrl: string;
  public serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';
  public establishment: EstablishmentInterface = {} as EstablishmentInterface;


  constructor(private http: HttpClient,
              private establishmentService: EstablishmentService,
              private snackbar: MatSnackBar,
              private router: Router,
              private activatedRoute: ActivatedRoute,) {

    if (this.establishmentService.isLoggedIn() === true) {
      this.setEstablishment();
    } else {
      this.router.navigate(['/login']);
    }

  }

  setEstablishment() {

    this.status = 'pending';
    const establishmentId: string = this.activatedRoute.snapshot.paramMap.get('establishmentId');

    this.http.get(this.serviceBaseUrl + '/establishment/' + establishmentId).subscribe( resEstablishment => {

      this.establishment = resEstablishment as EstablishmentInterface;
      this.status = 'success';

    });
  }

  addImageUrl() {

    if (this.imageUrl !== undefined && this.imageUrl !== '' && this.imageUrl !== null) {

        const msg = 'Url of the image added';

        if (this.establishment.albumImgUrls !== undefined) {
          this.establishment.albumImgUrls.unshift(this.imageUrl);
        } else {
          this.establishment.albumImgUrls = [this.imageUrl];
        }
        this.establishmentService.editEstablishment(this.establishment).subscribe(output => {

          this.imageUrl = '';
          this.setEstablishment(); // fetching the information so the freshly added image is displayed

          this.snackbar.open(msg, 'understood', {
            duration: 2500
          });    }, err => {
          console.log(err);
        });

    } else {
      alert('It appears that your url is empty. Make sure you enter a valid url.');
    }

  }

  deleteImageUrl(urlIndex: number) {

    const msg = 'Url of the image deleted';

    this.establishment.albumImgUrls.splice(urlIndex, 1);
    this.establishmentService.editEstablishment(this.establishment).subscribe(output => {

      this.setEstablishment(); // fetching the information so the freshly deleted image is not displayed anymore

      this.snackbar.open(msg, 'understood', {
        duration: 2500
      });    }, err => {
      console.log(err);
    });

  }

}
