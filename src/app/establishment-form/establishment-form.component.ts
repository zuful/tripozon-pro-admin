import { Component, Inject } from '@angular/core';
import { EstablishmentInterface } from '../../interfaces/establishment.interface';
import { MAT_DIALOG_DATA, MatDialogRef } from '@angular/material';
import {HttpClient, HttpHeaders} from '@angular/common/http';

@Component({
  selector: 'app-establishment-form',
  templateUrl: './establishment-form.component.html',
  styleUrls: ['./establishment-form.component.css']
})
export class EstablishmentFormComponent {

  public serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';

  constructor(private dialogRef: MatDialogRef<EstablishmentFormComponent>,
              @Inject(MAT_DIALOG_DATA) public establishment: EstablishmentInterface,
              private http: HttpClient) {

  }

  editEstablishment() {
    if (this.checkEmployeeForm() === true) {

      this.establishment.modificationDate = new Date(Date.now()).toISOString();

      this.http.patch(this.serviceBaseUrl + '/establishment/' + this.establishment.id,
        this.establishment,
        {headers: new HttpHeaders().set('Content-Type', 'application/json')}).subscribe(output => {
        this.dialogRef.close('edit');
      }, err => {
        console.log(err);
      });

    } else {
      alert('All fields must be filled.');
    }

  }

  checkEmployeeForm(): boolean {
    let isFormOk: boolean;

    if (this.establishment.email !== '') {
      isFormOk = true;
    } else {
      isFormOk = false;
    }

    return isFormOk;
  }
}
