import { Component } from '@angular/core';
import { EstablishmentInterface } from '../../interfaces/establishment.interface';
import { EstablishmentFormComponent } from '../establishment-form/establishment-form.component';
import { MatDialog, MatSnackBar} from '@angular/material';
import { HttpClient } from '@angular/common/http';
import { EstablishmentService } from '../../providers/establishment-service';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  selector: 'app-establishment-detail',
  templateUrl: './establishment-detail.component.html',
  styleUrls: ['./establishment-detail.component.css']
})
export class EstablishmentDetailComponent {
  public status = '';
  public serviceBaseUrl = 'https://tripozon-fortinbras.herokuapp.com';
  public establishmentId: string;
  public establishment: EstablishmentInterface = {} as EstablishmentInterface;

  constructor(private http: HttpClient,
              private dialog: MatDialog,
              private snackbar: MatSnackBar,
              private establishmentService: EstablishmentService,
              private router: Router,
              private activatedRoute: ActivatedRoute) {

    if (this.establishmentService.isLoggedIn() === true) {
      this.establishmentId = this.activatedRoute.snapshot.paramMap.get('establishmentId');
      this.setEstablishment();

    } else {
      this.router.navigate(['/login']);
    }
  }

  setEstablishment() {

    this.status = 'pending';

    this.http.get(this.serviceBaseUrl + '/establishment/' + this.establishmentId).subscribe( resEstablishment => {

      this.establishment = resEstablishment as EstablishmentInterface;
      console.log(this.establishment.description);
      this.status = 'success';

    });
  }

  openDialog() {
    const dialogRef = this.dialog.open(EstablishmentFormComponent, {
      width: '860px',
      data: this.establishment
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result !== undefined) {

        this.setEstablishment();
        const msg = 'Establishment successfully edited.';
        this.snackbar.open(msg, 'understood', {
          duration: 2500
        });

      }
      console.log('The dialog was closed');
    });
  }


}
